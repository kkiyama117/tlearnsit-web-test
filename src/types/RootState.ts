import { ThemeState } from 'styles/theme/types';

import { CanvasState } from 'app/shared/containers/Canvas/types';

import { CameraState } from 'app/shared/containers/Camera/types';

import { CanvasCameraState } from 'app/shared/containers/CanvasCamera/types';

import { CameraFormState } from 'app/shared/containers/CameraForm/types';

// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/* 
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/

export interface RootState {
  // Global states
  theme?: ThemeState;
  // Each containers
  canvas?: CanvasState;
  camera?: CameraState;
  canvasCamera?: CanvasCameraState;
  cameraForm?: CameraFormState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
