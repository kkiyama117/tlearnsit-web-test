/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const CameraPage = lazyLoad(
  () => import('./index'),
  module => module.CameraPage,
);
