import React from 'react';
import { Helmet } from 'react-helmet-async';
import { PageWrapper } from '../../shared/components/PageWrapper';
import { NavBar } from '../../shared/containers/NavBar';
import { NavLink } from 'react-router-dom';
import { Lead } from '../../shared/components/Lead';
import { Camera } from '../../shared/containers/Camera/Loadable';

export function CameraPage() {
  return (
    <>
      <Helmet>
        <title>Camera</title>
        <meta name="description" content="Camera page" />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <Lead>
          <strong>Take a Picture</strong>
        </Lead>
        <Camera size={{ width: 720, height: 480 }} />
        <NavLink to={'/'}>top</NavLink>
      </PageWrapper>
    </>
  );
}
