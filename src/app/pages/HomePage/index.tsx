import React from 'react';
import { Helmet } from 'react-helmet-async';
import { PageWrapper } from '../../shared/components/PageWrapper';
import { NavBar } from '../../shared/containers/NavBar';
import { NavLink } from 'react-router-dom';
import { P } from 'app/shared/components/P';
import { Lead } from '../../shared/components/Lead';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="Top page" />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <Lead>
          <strong>HomePage Container</strong>
        </Lead>
        <P>
          たまたま昔自分の勉強のために使ったTemplateにReact, Redux
          及びその周辺のPackageが使われていたものがあったので引っ張ってきました.
        </P>
        <P>
          <NavLink to={'/camera'}>camera</NavLink>
        </P>
        <P>
          <NavLink to={'/camera2'}>camera2</NavLink>
        </P>
        <P>
          <NavLink to={'/canvas'}>canvas</NavLink>
        </P>
        <P>
          <NavLink to={'/cc'}>canvas&camera</NavLink>
        </P>
      </PageWrapper>
    </>
  );
}
