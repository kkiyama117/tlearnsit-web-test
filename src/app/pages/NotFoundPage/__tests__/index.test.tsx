import React from 'react';
import { NotFoundPage } from '../index';
import { createRenderer } from 'react-test-renderer/shallow';

const shallowRenderer = createRenderer();

describe('<NotFoundPage />', () => {
  it('should match snapshot', () => {
    shallowRenderer.render(<NotFoundPage />);
    const renderedOutput = shallowRenderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });

  // TODO: Use mock
  // https://qiita.com/r_1105/items/ddcb1ac56df13a0c77ca
  // it('should should contain Link', () => {
  //   const notFoundPage = renderPage();
  //   expect(notFoundPage.root.findByType(Link)).toBeDefined();
  // });
});
