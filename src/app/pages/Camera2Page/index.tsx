import React from 'react';
import { Helmet } from 'react-helmet-async';
import { PageWrapper } from '../../shared/components/PageWrapper';
import { NavBar } from '../../shared/containers/NavBar';
import { NavLink } from 'react-router-dom';
import { Lead } from '../../shared/components/Lead';
import { CameraForm } from '../../shared/containers/CameraForm/Loadable';

export function Camera2Page() {
  return (
    <>
      <Helmet>
        <title>Camera</title>
        <meta name="description" content="Camera page" />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <Lead>
          <strong>Take a Picture</strong>
        </Lead>
        <CameraForm />
        <NavLink to={'/'}>top</NavLink>
      </PageWrapper>
    </>
  );
}
