import React from 'react';
import { Helmet } from 'react-helmet-async';
import { PageWrapper } from '../../shared/components/PageWrapper';
import { NavBar } from '../../shared/containers/NavBar';
import { NavLink } from 'react-router-dom';
import { Lead } from '../../shared/components/Lead';
import { CanvasCamera } from '../../shared/containers/CanvasCamera';
import { P } from '../../shared/components/P';

export function CanvasCameraPage() {
  return (
    <>
      <Helmet>
        <title>Canvas Camera Page</title>
        <meta name="description" content="CC page" />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <Lead>
          <strong>Take a Picture</strong>
          <P>作業中です</P>
        </Lead>
        <CanvasCamera />
        <NavLink to={'/'}>top</NavLink>
      </PageWrapper>
    </>
  );
}
