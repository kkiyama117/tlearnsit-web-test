import React from 'react';
import { Helmet } from 'react-helmet-async';
import { PageWrapper } from '../../shared/components/PageWrapper';
import { NavBar } from '../../shared/containers/NavBar';
import { NavLink } from 'react-router-dom';
import { Lead } from '../../shared/components/Lead';
// import Canvas from '../../shared/containers/Canvas/old';
import { Canvas } from '../../shared/containers/Canvas/Loadable';

export function CanvasPage() {
  return (
    <>
      <Helmet>
        <title>Canvas</title>
        <meta name="description" content="Camera page" />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <Lead>
          <strong>Take a comment</strong>
        </Lead>
        <Canvas width={720} height={480} />
        <NavLink to={'/'}>top</NavLink>
      </PageWrapper>
    </>
  );
}
// ref={canvasRef
