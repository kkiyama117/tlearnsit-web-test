/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const CanvasPage = lazyLoad(
  () => import('./index'),
  module => module.CanvasPage,
);
