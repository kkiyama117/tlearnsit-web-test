import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState, LineType, Path, Point2D } from './types';

// The initial state of the Canvas container
export const initialState: ContainerState = {
  drawing: false,
  canvasPosition: { x: 0, y: 0 },
  currentLineMode: 'BLACK',
  currentPath: [],
  pathData: [],
};

const canvasSlice = createSlice({
  name: 'canvas',
  initialState,
  reducers: {
    // ALL
    initializeAction(state, action: PayloadAction) {
      state.drawing = false;
      state.currentLineMode = 'BLACK';
      state.currentPath = [];
      state.pathData = [];
    },
    // CanvasAttr
    startDrawingAction(state, action: PayloadAction) {
      state.drawing = true;
    },
    endDrawingAction(state, action: PayloadAction) {
      state.drawing = false;
    },
    // Canvas Position
    setCanvasPositionAction(state, action: PayloadAction<Point2D>) {
      state.canvasPosition = action.payload;
    },
    resetCanvasPositionAction(state, action: PayloadAction) {
      state.canvasPosition = { x: 0, y: 0 };
    },
    setLineTypeAction(state, action: PayloadAction<LineType>) {
      state.currentLineMode = action.payload;
    },
    // Current Path
    addPointToPathAction(state, action: PayloadAction<Point2D>) {
      console.log('new point:', action.payload);
      state.currentPath = state.currentPath.concat([action.payload]);
      console.log(
        'point add to current path ...',
        state.currentPath[state.currentPath.length - 1].x,
        state.currentPath[state.currentPath.length - 1].y,
      );
      console.log('count of points in this path:', state.currentPath.length);
    },
    endPathAction(state, action: PayloadAction) {},
    resetPathAction(state, action: PayloadAction) {
      state.currentPath = [];
    },
    // All Path Arrays
    addPathToListAction(state, action: PayloadAction<Path>) {
      state.pathData = state.pathData.concat([action.payload]);
      console.log('count of all path array:', state.pathData.length);
    },
  },
});

export const { actions, reducer, name: sliceKey } = canvasSlice;
