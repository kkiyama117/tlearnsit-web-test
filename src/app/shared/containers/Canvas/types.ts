/* --- STATE --- */
export interface CanvasState {
  drawing: boolean;
  canvasPosition: Point2D;
  currentPath: PathArray;
  currentLineMode: LineType;
  pathData: MultiPath;
}

export type CanvasSize = {
  width: number;
  height: number;
};

// Data Types
export type Point2D = {
  x: number;
  y: number;
};

export const lineType = {
  BLACK: null,
  ERASE: null,
};
// Use Union type to use struct type check
export type LineType = keyof typeof lineType;

export type PathArray = Point2D[];

export type Path = {
  type: LineType;
  path: PathArray;
};
export type MultiPath = Path[];

export type ContainerState = CanvasState;
