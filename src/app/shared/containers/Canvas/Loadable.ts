/**
 *
 * Asynchronously loads the component for Canvas
 *
 */

import { lazyLoad } from 'utils/loadable';

export const Canvas = lazyLoad(
  () => import('./index'),
  module => module.Canvas,
);
