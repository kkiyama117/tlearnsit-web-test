import { Path, Point2D } from './types';
import { RefObject } from 'react';

export function getPositionOfElement(element: HTMLElement): Point2D {
  return {
    x: element.getBoundingClientRect().left,
    y: element.getBoundingClientRect().top,
  };
}

/// if you can't get, return undefined
export const getCanvasCtx = (
  canvasRef: RefObject<HTMLCanvasElement> | null,
) => {
  if (canvasRef !== null) {
    const _canvas: any = canvasRef.current;
    const _ctx: CanvasRenderingContext2D = _canvas.getContext('2d');
    if (_ctx !== null) {
      return _ctx;
    }
  }
};

export const pathToSVGPathDataString = (path: Path) => {
  const _array = path.path;
  return _array
    .map(
      (value, index) => (index === 0 ? 'M ' : 'L ') + value.x + ' ' + value.y,
    )
    .join(' ');
};
