// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { actions } from './slice';
import { selectLineMode, selectCurrentPath } from './selectors';

/// 描画終了時に処理をする
export function* endDrawing() {
  // Arrayに足す.
  const pathMode = yield select(selectLineMode);
  const pathArray = yield select(selectCurrentPath);
  yield put(
    actions.addPathToListAction({
      type: pathMode,
      path: pathArray,
    }),
  );
  // ListのReset
  yield put(actions.resetPathAction());
}

export function* canvasSaga() {
  yield takeLatest(actions.endDrawingAction.type, endDrawing);
}
