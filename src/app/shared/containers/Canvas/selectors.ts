import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.canvas || initialState;

export const selectDrawing = createSelector(
  [selectDomain],
  canvasState => canvasState.drawing,
);

export const selectCanvasPosition = createSelector(
  [selectDomain],
  canvasState => canvasState.canvasPosition,
);

export const selectLineMode = createSelector(
  [selectDomain],
  canvasState => canvasState.currentLineMode,
);

export const selectCurrentPath = createSelector(
  [selectDomain],
  canvasState => canvasState.currentPath,
);

export const selectPathData = createSelector(
  [selectDomain],
  canvasState => canvasState.pathData,
);
