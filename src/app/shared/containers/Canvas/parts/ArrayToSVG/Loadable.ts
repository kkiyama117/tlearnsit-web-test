/**
 *
 * Asynchronously loads the component for ArrayToSvg
 *
 */

import { lazyLoad } from 'utils/loadable';

export const ArrayToSVG = lazyLoad(
  () => import('./index'),
  module => module.ArrayToSVG,
);
