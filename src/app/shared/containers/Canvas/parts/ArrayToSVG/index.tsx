/**
 *
 * ArrayToSVG
 *
 */
import React from 'react';
import styled from 'styled-components/macro';
import { Path } from '../../types';
import { pathToSVGPathDataString } from '../../utils';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import {
  selectLineMode,
  selectCurrentPath,
  selectPathData,
} from '../../selectors';

interface Props {
  width: number;
  height: number;
}

const PathToJSX = (path: Path, index: number) => {
  const _base = (color: string) => (
    <path
      key={index}
      d={pathToSVGPathDataString(path)}
      stroke={color}
      fill="transparent"
      strokeWidth="2"
    />
  );
  switch (path.type) {
    case 'BLACK':
      return _base('black');
    case 'ERASE':
      return _base('white');
  }
};

export function ArrayToSVG(props: Props) {
  const pathData = useSelector(selectPathData);
  const currentPathArray = useSelector(selectCurrentPath);
  const currentPathMode = useSelector(selectLineMode);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  return (
    <div>
      <StyledSVG width={props.width} height={props.height}>
        {pathData.map(PathToJSX)}
        <PathToJSX type={currentPathMode} path={currentPathArray} />
      </StyledSVG>
    </div>
  );
}

const StyledSVG = styled.svg`
  border: 3px solid;
  display: block;
  background-color: transparent;
  border-color: ${p => p.theme.border};
`;
