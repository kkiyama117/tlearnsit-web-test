import React, { RefObject } from 'react';
import {
  useInjectReducer,
  useInjectSaga,
} from '../../../../../utils/redux-injectors';
import { actions, reducer, sliceKey } from '../slice';
import { canvasSaga } from '../saga';
import { useDispatch } from 'react-redux';
import { Button } from '../../../components/Button';
import { getCanvasCtx } from '../utils';

interface Props {
  width: number;
  height: number;
  canvasRef: RefObject<HTMLCanvasElement>;
}

export function ClearCanvasButton(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: canvasSaga });

  // check is Drawing
  const dispatch = useDispatch();
  const clearCanvas = () => {
    const ctx = getCanvasCtx(props.canvasRef);
    if (ctx !== undefined) {
      ctx.clearRect(0, 0, props.width, props.height);
      ctx.save();
      dispatch(actions.initializeAction());
    }
  };
  return <Button onClick={clearCanvas}>Clear All</Button>;
}
