import React, { RefObject, useEffect } from 'react';
import {
  useInjectReducer,
  useInjectSaga,
} from '../../../../../utils/redux-injectors';
import { actions, reducer, sliceKey } from '../slice';
import { canvasSaga } from '../saga';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from '../../../components/Button';
import { getCanvasCtx } from '../utils';
import { selectLineMode } from '../selectors';

interface Props {
  canvasRef: RefObject<HTMLCanvasElement> | null;
}

export function ToggleCanvasMode(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: canvasSaga });
  const drawingMode = useSelector(selectLineMode);
  const dispatch = useDispatch();

  // 副作用としてDrawingModeの変更時にCanvasの状態を合わせる
  useEffect(() => {
    const _ctx = getCanvasCtx(props.canvasRef);
    if (_ctx !== undefined) {
      switch (drawingMode) {
        case 'BLACK':
          _ctx.globalCompositeOperation = 'source-over';
          break;
        case 'ERASE':
          _ctx.globalCompositeOperation = 'destination-out';
          break;
      }
    }
  });

  // 逆にする
  const toggleDeleteMode = () => {
    switch (drawingMode) {
      case 'BLACK':
        dispatch(actions.setLineTypeAction('ERASE'));
        break;
      case 'ERASE':
        dispatch(actions.setLineTypeAction('BLACK'));
        break;
    }
  };

  const CheckLineMode = () => {
    switch (drawingMode) {
      case 'BLACK':
        return <div>線画モード: 黒</div>;
      case 'ERASE':
        return <div>消しゴムモード</div>;
    }
  };
  return (
    <>
      <Button onClick={toggleDeleteMode}>Toggle</Button>
      <CheckLineMode />
    </>
  );
}
