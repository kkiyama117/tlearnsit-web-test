/**
 *
 * Canvas
 *
 */

import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { actions, reducer, sliceKey } from './slice';
import { selectDrawing } from './selectors';
import { canvasSaga } from './saga';
import { Point2D } from './types';
import { getPositionOfElement } from './utils';
import { ClearCanvasButton } from './parts/ClearCanvasButton';
import { ToggleCanvasMode } from './parts/ToggleCanvasMode';
import { ArrayToSVG } from './parts/ArrayToSVG/Loadable';

interface Props {
  width: number;
  height: number;
}

export function Canvas(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: canvasSaga });

  // check is Drawing
  const isDrawing = useSelector(selectDrawing);
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  // canvasの参照を取得
  const _canvasRef = useRef<HTMLCanvasElement>(null);

  // initialize and destruct
  const useEffectOnMount = (effect: React.EffectCallback) => {
    useEffect(effect, []);
  };
  useEffectOnMount(() => {
    const _canvas: any = _canvasRef.current;
    // Keep Canvas position
    dispatch(actions.setCanvasPositionAction(getPositionOfElement(_canvas)));
    // Destruct
    return () => {
      dispatch(actions.resetCanvasPositionAction());
    };
  });

  // 関数群
  const handleDrawing = event => {
    if (isDrawing) {
      const point: Point2D = {
        x: event.nativeEvent.offsetX,
        y: event.nativeEvent.offsetY,
      };
      // Call Canvas Event
      dispatch(actions.addPointToPathAction(point));
      // Draw on canvas
      const _canvas: any = _canvasRef.current;
      const _ctx = _canvas.getContext('2d');
      _ctx.lineTo(point.x, point.y);
      _ctx.stroke();
    }
  };

  const startDrawing = event => {
    dispatch(actions.startDrawingAction());
    // Pointer Capture
    const _canvas: any = _canvasRef.current;
    _canvas.setPointerCapture(event.pointerId);
    // Draw on canvas
    const _ctx = _canvas.getContext('2d');
    _ctx.beginPath();
  };

  const endDrawing = event => {
    // Release Capture
    const _canvas: any = _canvasRef.current;
    _canvas.releasePointerCapture(event.pointerId);
    dispatch(actions.endDrawingAction());
  };

  return (
    <Div>
      <CanvasStyle
        {...props}
        width={props.width}
        height={props.height}
        ref={_canvasRef}
        onPointerUpCapture={e => endDrawing(e)}
        onPointerMoveCapture={e => handleDrawing(e)}
        onPointerDownCapture={e => startDrawing(e)}
      />
      <ToggleCanvasMode canvasRef={_canvasRef} />
      <ClearCanvasButton {...props} canvasRef={_canvasRef} />
      <ArrayToSVG width={props.width} height={props.height} />
    </Div>
  );
}

// https://qiita.com/yukiB/items/0e05c8bb5d477896051d#canvas%E3%81%AFcss%E3%81%A7%E3%82%B5%E3%82%A4%E3%82%BA%E3%82%92 \
// %E6%8C%87%E5%AE%9A%E3%81%97%E3%81%A6%E3%81%AF%E3%81%84%E3%81%91%E3%81%AA%E3%81%84
const CanvasStyle = styled.canvas`
  border: 3px solid;
  display: block;
  background-color: transparent;
  border-color: ${p => p.theme.border};
`;

const Div = styled.div``;
