/**
 *
 * Camera
 *
 */
import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectSrc } from './selectors';
import { cameraSaga } from './saga';
import Webcam, { WebcamProps } from 'react-webcam';
import { CameraSize } from './types';
import { ShowImg } from '../../components/ShowImg';
import { Button } from '../../components/Button';

interface Props {
  config?: WebcamProps;
  size: CameraSize;
}

export const Camera = memo((props: Props) => {
  // react hooks
  const webcamRef = React.useRef(null);
  // redux
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: cameraSaga });

  const imageSrc = useSelector(selectSrc);
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  // ReduxのActionを結びつける.
  const takePicture = () => {
    dispatch(actions.takeImageSrcAction(webcamRef.current));
  };
  const resetPicture = () => {
    dispatch(actions.resetImageSrcAction());
  };

  return (
    <Div>
      <Webcam
        audio={false}
        ref={webcamRef}
        screenshotFormat="image/webp"
        videoConstraints={props.size}
      />
      <Button onClick={takePicture}>Capture photo</Button>
      <Button onClick={resetPicture}>Remove photo</Button>
      <ShowImg imageSrc={imageSrc} />
    </Div>
  );
});

const Div = styled.div``;

// React版
/*
 import React from 'react';
 import Webcam from 'react-webcam';
 import { ShowImg } from '../ShowImg';

 const videoConstraints = {
   width: 1280,
   height: 720,
   facingMode: 'user',
 };

 export const Camera = (setting: typeof videoConstraints) => {
   const webcamRef = React.useRef(null);
   const [imgSrc, setImgSrc] = React.useState('');

   const capture = React.useCallback(() => {
     const camera = webcamRef.current;
     if (camera !== null) {
       const imageSrc = (camera as Webcam).getScreenshot();
       if (imageSrc !== null) {
         setImgSrc(imageSrc);
       }
     }
   }, [webcamRef, setImgSrc]);

   return (
     <>
       <Webcam
         audio={false}
         height={setting.height}
         ref={webcamRef}
         screenshotFormat="image/jpeg"
         width={setting.width}
         videoConstraints={videoConstraints}
       />
       <button onClick={capture}>Capture photo</button>
       <ShowImg imageSrc={imgSrc} />
     </>
   );
 };
*/
