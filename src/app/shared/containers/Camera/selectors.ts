import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.camera || initialState;

export const selectSrc = createSelector(
  [selectDomain],
  cameraState => cameraState.imageSrc,
);
