import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';
import Webcam from 'react-webcam';

// The initial state of the Camera container
export const initialState: ContainerState = {
  imageSrc: null,
};

const cameraSlice = createSlice({
  name: 'camera',
  initialState,
  reducers: {
    takeImageSrcAction(
      state: ContainerState,
      action: PayloadAction<Webcam | null>,
    ) {},
    setImageSrcAction(state: ContainerState, action: PayloadAction<string>) {
      state.imageSrc = action.payload;
    },
    resetImageSrcAction(state) {
      state.imageSrc = null;
    },
  },
});

export const { actions, reducer, name: sliceKey } = cameraSlice;
