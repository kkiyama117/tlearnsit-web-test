/* --- STATE --- */
export interface CameraState {
  imageSrc: string | null;
}

export type CameraSize = {
  width: number;
  height: number;
};

export type ContainerState = CameraState;
