import { put, takeLatest } from 'redux-saga/effects';
// import { take, call, select } from 'redux-saga/effects';
import { actions } from './slice';

export function* takeAPicture(action) {
  // Webcam を action から取得
  const webcam = action.payload;
  // 写真撮影
  if (webcam != null) {
    const imageSrc = webcam.getScreenshot();
    if (imageSrc !== null) {
      // srcにセット
      yield put(actions.setImageSrcAction(imageSrc));
    }
  }
}

export function* cameraSaga() {
  yield takeLatest(actions.takeImageSrcAction.type, takeAPicture);
  // APIServerに送る, そちらが完成したら処理を書く
  // yield takeLatest(actions.setImageSrcAction.type, takeAPicture);
}
