/**
 *
 * Asynchronously loads the component for Camera
 *
 */

import { lazyLoad } from 'utils/loadable';

export const Camera = lazyLoad(
  () => import('./index'),
  module => module.Camera,
);
