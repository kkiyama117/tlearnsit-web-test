import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the CameraForm container
export const initialState: ContainerState = {};

const cameraFormSlice = createSlice({
  name: 'cameraForm',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const { actions, reducer, name: sliceKey } = cameraFormSlice;
