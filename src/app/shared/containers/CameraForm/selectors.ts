import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.cameraForm || initialState;

export const selectCameraForm = createSelector(
  [selectDomain],
  cameraFormState => cameraFormState,
);
