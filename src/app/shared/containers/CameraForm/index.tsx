/**
 *
 * CameraForm
 *
 */

import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectCameraForm } from './selectors';
import { cameraFormSaga } from './saga';
import { Formik } from 'formik';
import { Button } from '../../components/Button';

interface Props {}

interface ErrorProps {
  photo?: string;
}

export interface MediaUploadState {
  progress: number;
  file?: File;
  error?: string;
}

export function CameraForm(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: cameraFormSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const cameraForm = useSelector(selectCameraForm);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const [photo, setPhoto] = React.useState<File>();
  const [error, setError] = React.useState<ErrorProps>();
  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.files) {
      return;
    }
    let file = e.target.files[0];
    setPhoto(file);
    setError({ photo: undefined });
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const handleRemoveImage = () => {
    setPhoto(undefined);
  };
  const onSubmit = () => {
    setTimeout((e: React.ChangeEvent<HTMLInputElement>) => {
      alert(JSON.stringify(photo?.name, null, 2));
    }, 400);
  };

  return (
    <>
      <Div>
        <Formik
          initialValues={{ photo: '' }}
          validate={_ => {
            const errors: ErrorProps = {};
            if (error?.photo) {
              errors.photo = 'ERROR';
            }
            return errors;
          }}
          onSubmit={onSubmit}
        >
          {({
            values,
            errors,
            touched,
            handleSubmit,
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <input
                type="file"
                name="image"
                accept="image/*"
                capture="user"
                onChange={handleFileChange}
                value={values.photo}
              />
              {errors.photo && touched.photo && errors.photo}
              <Button type="submit" onClick={handleSubmit}>
                Submit
              </Button>
              <Thumb file={photo} />
            </form>
          )}
        </Formik>
      </Div>
    </>
  );
}

interface ThumbProps {
  loading?: boolean;
  file?: File;
}

const Thumb = (props: ThumbProps) => {
  const [thumb, setThumb] = React.useState<string | ArrayBuffer>();
  React.useEffect(() => {
    const reader = new FileReader();
    reader.onloadend = () => {
      const result = reader.result;
      if (result != null) {
        setThumb(result);
      }
    };
    const file = props.file;
    if (file) {
      reader.readAsDataURL(file);
    }
  }, [props]);
  if (thumb === null || thumb === undefined) {
    return <p>loading...</p>;
  } else {
    return (
      <Img
        src={thumb.toString()}
        className="img-thumbnail mt-2"
        width={'720px'}
        height={'480px'}
      />
    );
  }
};

const Div = styled.div``;
const Img = styled.img``;
