/**
 *
 * Asynchronously loads the component for CameraForm
 *
 */

import { lazyLoad } from 'utils/loadable';

export const CameraForm = lazyLoad(
  () => import('./index'),
  module => module.CameraForm,
);
