import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.canvasCamera || initialState;

export const selectCanvasCamera = createSelector(
  [selectDomain],
  canvasCameraState => canvasCameraState,
);
