/**
 *
 * CanvasCamera
 *
 */

import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectCanvasCamera } from './selectors';
import { canvasCameraSaga } from './saga';
import { Canvas } from '../Canvas/Loadable';
import { Camera } from '../Camera/Loadable';

interface Props {}

export function CanvasCamera(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: canvasCameraSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const canvasCamera = useSelector(selectCanvasCamera);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  return (
    <>
      <Div>
        <CameraDiv>
          <Camera size={{ width: 720, height: 480 }} />
        </CameraDiv>
        <CanvasDiv>
          <Canvas width={720} height={480} />
        </CanvasDiv>
      </Div>
    </>
  );
}
const Div = styled.div``;

const CameraDiv = styled.div`
  position: absolute;
`;
const CanvasDiv = styled.div`
  position: absolute;
  top: 480px;
`;
