import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the CanvasCamera container
export const initialState: ContainerState = {};

const canvasCameraSlice = createSlice({
  name: 'canvasCamera',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const { actions, reducer, name: sliceKey } = canvasCameraSlice;
