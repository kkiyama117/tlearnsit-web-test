/**
 *
 * Asynchronously loads the component for CampusBoard
 *
 */

import { lazyLoad } from 'utils/loadable';

export const CampusBoard = lazyLoad(
  () => import('./index'),
  module => module.CampusBoard,
);
