import React from 'react';
import { render } from '@testing-library/react';

import { CampusBoard } from '..';

describe('<CampusBoard  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<CampusBoard />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
