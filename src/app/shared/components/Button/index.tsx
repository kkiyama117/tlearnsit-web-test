/**
 *
 * Button
 *
 */
// import React from 'react';
import styled from 'styled-components/macro';
import React, { ReactNode } from 'react';

interface Props {
  children: ReactNode;
  type?: any;
  onClick?: () => void;
  disabled?: boolean;
}

export function Button(props: Props) {
  return (
    <>
      <TestButton
        type="button"
        onClick={props.onClick}
        disabled={props.disabled ?? false}
      >
        {props.children}
      </TestButton>
    </>
  );
}

const TestButton = styled.button`
  border-radius: 5%; /* 角丸       */
  cursor: pointer; /* カーソル   */
  background: ${p => p.theme.secondary}; /* 背景色     */
  color: ${p => p.theme.text}; /* 文字色     */
  transition: 0.3s; /* なめらか変化 */
  box-shadow: 6px 6px 3px ${p => p.theme.borderLight}; /* 影の設定 */
  border: 2px solid ${p => p.theme.border}; /* 枠の指定 */
  &:hover {
    box-shadow: none; /* カーソル時の影消去 */
  }
`;
