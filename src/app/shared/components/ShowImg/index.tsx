/**
 *
 * ShowImg
 *
 */
import React from 'react';
import styled from 'styled-components/macro';
import { useTranslation } from 'react-i18next';
import { translations } from '../../../../locales/i18n';

interface Props {
  imageSrc: string | null;
}

export function ShowImg(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <Div>
      {props.imageSrc && (
        <img
          src={props.imageSrc}
          alt={t(translations.pages.showImg.please_enable_camera())}
        />
      )}
    </Div>
  );
}

const Div = styled.div``;
