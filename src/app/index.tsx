/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './pages/HomePage/Loadable';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { CameraPage } from './pages/CameraPage/Loadable';
import { CanvasPage } from './pages/CanvasPage/Loadable';
import { CanvasCameraPage } from './pages/CanvasCameraPage';
import { Camera2Page } from './pages/Camera2Page/Loadable';

export function App() {
  return (
    <Router basename={'/kiyama'}>
      <Helmet
        titleTemplate="%s - camera demo homepage"
        defaultTitle="camera demo homepage"
      >
        <meta name="description" content="Demo page to use web camera" />
      </Helmet>

      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/camera" component={CameraPage} />
        <Route path="/camera2" component={Camera2Page} />
        <Route path="/canvas" component={CanvasPage} />
        <Route path="/cc" component={CanvasCameraPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </Router>
  );
}
