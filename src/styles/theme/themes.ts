// export to use storybook only.
// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=1B5E20&secondary.color=FFEB3B
export const defaultTheme = {
  primary: 'rgb(255,235,59)',
  secondary: 'rgb(26,26,255)',
  text: 'rgba(241,233,231,1)',
  textSecondary: 'rgba(241,233,231,0.6)',
  background: 'rgb(27,94,32)',
  backgroundVariant: 'rgb(0,51,0)',
  border: 'rgba(241,233,231,0.15)',
  borderLight: 'rgba(241,233,231,0.05)',
};

// export to use storybook only.
export const lightTheme: Theme = {
  primary: 'rgba(215,113,88,1)',
  secondary: 'rgb(65,179,75)',
  text: 'rgba(58,52,51,1)',
  textSecondary: 'rgba(58,52,51,0.7)',
  background: 'rgba(255,255,255,1)',
  backgroundVariant: 'rgba(251,249,249,1)',
  border: 'rgba(58,52,51,0.12)',
  borderLight: 'rgba(58,52,51,0.05)',
};

// export to use storybook only.
export const darkTheme: Theme = {
  primary: 'rgba(220,120,95,1)',
  secondary: 'rgb(105,75,174)',
  text: 'rgba(241,233,231,1)',
  textSecondary: 'rgba(241,233,231,0.6)',
  background: 'rgba(0,0,0,1)',
  backgroundVariant: 'rgba(28,26,26,1)',
  border: 'rgba(241,233,231,0.15)',
  borderLight: 'rgba(241,233,231,0.05)',
};

export type Theme = typeof defaultTheme;

export const themes = {
  default: defaultTheme,
  light: lightTheme,
  dark: darkTheme,
};
