# 撮ってビューのテスト 及び Gitlab テスト

## Gitlab の導入方法 / 問題の提起と解決 / コードの更新

[`Contribute.md`](./CONTRIBUTING.md) を参照

## commands

以下のコマンドで開発用の localhost のサーバーが起動する.

```shell script
# install packages
yarn --dev
# start dev server
# Default port is 8000 (you can change. see `.env.local`)
# Ctrl-c で終了.
yarn start
```

story-book の起動, test, 新しい Conponent の作成は以下.

```shell script
# See Storybook
yarn storybook
# test
yarn test
# Generate new Components
yarn generate
```

静的なページの作成には以下のコマンド.

```shell script
# build to deploy
yarn build
```

## commit

自動で Linter や Formatter が走ります. 問題があると Commit が Reject されますので, 適宜修正してください.
