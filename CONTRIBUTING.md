# Contribution

Gitlab での開発の流れ及びこの Repository での開発について
http://arch:8800/snippets/2 にもある通り、最低限になっているため, [Gitlab 公式の Document](https://docs.gitlab.com/ee/)も参照してください.

## Gitlab の導入

-> http://arch:8800/snippets/2

## 問題提起 / 議論

Redmine 等における `ticket` にあたるのが `issue`.
左手サイドバーの `issue`(日本語だと `課題`)がそれに当たる.
`リスト`で一覧, `board` で看板 の形式でそれぞれ確認できる. また, マイルストーンで期限や進捗ごとの整理も出来る.
リスト画面の右側 `New Issue(新規課題)` ボタンや 上部メニューバーの `+` ボタンなどから追加出来る.

## コードの更新の反映

Github の `pull request` にあたるのが `merge request`. git から push した commit(branch)を master に反映させるためのもの.
左のメニューバー `Merge Request(マージリクエスト)` から`新規マージリクエスト` で作成し, Review 等の後, Web 上で Merge する.
こちらもマイルストーン等がつけられるので出来る限りつけて管理したい.
