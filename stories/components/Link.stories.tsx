import * as React from 'react';
// import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';
import { Link } from '../../src/app/shared/components/Link';
import { BrowserRouter } from 'react-router-dom';

export default {
  title: 'Link',
  component: Link,
};
export const defaultA = () => (
  <ThemeProvider theme={defaultTheme}>
    <BrowserRouter>
      <div style={{ backgroundColor: defaultTheme.background }}>
        <Link to={''}>test</Link>
      </div>
    </BrowserRouter>
  </ThemeProvider>
);

export const whiteA = () => (
  <ThemeProvider theme={lightTheme}>
    <BrowserRouter>
      <div style={{ backgroundColor: lightTheme.background }}>
        <Link to={''}>test</Link>
      </div>
    </BrowserRouter>
  </ThemeProvider>
);

export const blackA = () => (
  <ThemeProvider theme={darkTheme}>
    <BrowserRouter>
      <div style={{ backgroundColor: darkTheme.background }}>
        <Link to={''}>test</Link>
      </div>
    </BrowserRouter>
  </ThemeProvider>
);
