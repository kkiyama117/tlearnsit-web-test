import * as React from 'react';
// import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';
import { A } from '../../src/app/shared/components/A';
import { P } from '../../src/app/shared/components/P';
import { Lead } from '../../src/app/shared/components/Lead';

export default {
  title: 'Lead',
  component: Lead,
};
export const defaultLead = () => (
  <ThemeProvider theme={defaultTheme}>
    <div style={{ backgroundColor: defaultTheme.background }}>
      <Lead>
        test <strong>strong</strong>
      </Lead>
    </div>
  </ThemeProvider>
);

export const whiteLead = () => (
  <ThemeProvider theme={lightTheme}>
    <div style={{ backgroundColor: lightTheme.background }}>
      <Lead>
        test <strong>strong</strong>
      </Lead>
    </div>
  </ThemeProvider>
);

export const blackLead = () => (
  <ThemeProvider theme={darkTheme}>
    <div style={{ backgroundColor: darkTheme.background }}>
      <Lead>
        test <strong>strong</strong>
      </Lead>
    </div>
  </ThemeProvider>
);
