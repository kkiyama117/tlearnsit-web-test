import * as React from 'react';
// import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';
import { A } from '../../src/app/shared/components/A';
import { P } from '../../src/app/shared/components/P';
import { Lead } from '../../src/app/shared/components/Lead';

export default {
  title: 'P',
  component: P,
};
export const defaultP = () => (
  <ThemeProvider theme={defaultTheme}>
    <div style={{ backgroundColor: defaultTheme.background }}>
      <P>
        test <strong>strong</strong>
      </P>
    </div>
  </ThemeProvider>
);

export const whiteP = () => (
  <ThemeProvider theme={lightTheme}>
    <div style={{ backgroundColor: lightTheme.background }}>
      <P>
        test <strong>strong</strong>
      </P>
    </div>
  </ThemeProvider>
);

export const blackP = () => (
  <ThemeProvider theme={darkTheme}>
    <div style={{ backgroundColor: darkTheme.background }}>
      <P>
        test <strong>strong</strong>
      </P>
    </div>
  </ThemeProvider>
);
