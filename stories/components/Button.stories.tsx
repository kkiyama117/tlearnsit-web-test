import React from 'react';
import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import { Button } from '../../src/app/shared/components/Button';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';

export default {
  title: 'Button',
  component: Button,
};

export const defaultButton = () => (
  <ThemeProvider theme={defaultTheme}>
    <div style={{ backgroundColor: defaultTheme.background }}>
      <Button>test</Button>
    </div>
  </ThemeProvider>
);

export const whiteButton = () => (
  <ThemeProvider theme={lightTheme}>
    <div style={{ backgroundColor: lightTheme.background }}>
      <Button>test</Button>
    </div>
  </ThemeProvider>
);

export const blackButton = () => (
  <ThemeProvider theme={darkTheme}>
    <div style={{ backgroundColor: darkTheme.background }}>
      <Button>test</Button>
    </div>
  </ThemeProvider>
);

export const Emoji = () => (
  <ThemeProvider theme={lightTheme}>
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  </ThemeProvider>
);
