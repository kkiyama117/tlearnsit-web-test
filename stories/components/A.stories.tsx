import * as React from 'react';
// import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';
import { A } from '../../src/app/shared/components/A';
import { P } from '../../src/app/shared/components/P';

export default {
  title: 'A',
  component: A,
};

export const defaultA = () => (
  <ThemeProvider theme={defaultTheme}>
    <div style={{ backgroundColor: defaultTheme.background }}>
      <A>test</A>
    </div>
  </ThemeProvider>
);

export const whiteA = () => (
  <ThemeProvider theme={lightTheme}>
    <div style={{ backgroundColor: lightTheme.background }}>
      <A>test</A>
    </div>
  </ThemeProvider>
);

export const blackA = () => (
  <ThemeProvider theme={darkTheme}>
    <div style={{ backgroundColor: darkTheme.background }}>
      <A>test</A>
    </div>
  </ThemeProvider>
);
