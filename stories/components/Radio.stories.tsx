import React from 'react';
import { action } from '@storybook/addon-actions';
import { ThemeProvider } from 'styled-components';
import { Button } from '../../src/app/shared/components/Button';
import {
  darkTheme,
  defaultTheme,
  lightTheme,
} from '../../src/styles/theme/themes';
import { Radio } from '../../src/app/shared/components/Radio';
import styled from 'styled-components/macro';
import { FormLabel } from '../../src/app/shared/components/FormLabel';

export default {
  title: 'Radio',
  component: Radio,
};
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${FormLabel} {
    margin-bottom: 0.625rem;
  }
`;
const Languages = styled.div`
  display: flex;
  .radio {
    margin-right: 1.5rem;
  }
`;

export const defaultRadioButton = () => (
  <ThemeProvider theme={defaultTheme}>
    <div style={{ backgroundColor: defaultTheme.background }}>
      <Wrapper>
        <Languages>
          <Radio
            id="ja"
            label="Japanese"
            className="radio"
            name="language"
            value="ja"
          />
          <Radio
            id="en"
            label="English"
            className="radio"
            name="language"
            value="ja"
          />
        </Languages>
      </Wrapper>
    </div>
  </ThemeProvider>
);

export const whiteRadioButton = () => (
  <ThemeProvider theme={lightTheme}>
    <div style={{ backgroundColor: lightTheme.background }}>
      <Wrapper>
        <Languages>
          <Radio
            id="ja"
            label="Japanese"
            className="radio"
            name="language"
            value="ja"
          />
          <Radio
            id="en"
            label="English"
            className="radio"
            name="language"
            value="ja"
          />
        </Languages>
      </Wrapper>
    </div>
  </ThemeProvider>
);

export const blackRadioButton = () => (
  <ThemeProvider theme={darkTheme}>
    <div style={{ backgroundColor: darkTheme.background }}>
      <Wrapper>
        <Languages>
          <Radio
            id="ja"
            label="Japanese"
            className="radio"
            name="language"
            value="ja"
          />
          <Radio
            id="en"
            label="English"
            className="radio"
            name="language"
            value="ja"
          />
        </Languages>
      </Wrapper>
    </div>
  </ThemeProvider>
);
